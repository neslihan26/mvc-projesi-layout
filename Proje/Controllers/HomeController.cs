﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proje.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Anasayfa()
        {
            return View();
        }
        public ActionResult Ilanlar()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult EvcilHayvanBakimi()
        {
            return View();
        }
            
         public ActionResult SahiplendirmeBilgileri()
        {
            return View();
        }
 
        public ActionResult FotoVideo()
        {
            return View();
        }
    
    }
}